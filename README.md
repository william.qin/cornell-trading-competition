# Cornell Trading Competition

Jump to the "Using Our Server" section to jump straight into running a strategy with Apifiny Algo

## Registration

1. Registration will be handled by making an account on our server for each competitor. As detailed below, this is how submission will be handled.

2. To this end, participant email addresses should be collected by the organizers upon registration and sent to us so that we can set accounts up; this consists of adding a new user to the server that we are providing and adding the relevant folders.

## General details

1. The data for this competition is provided as if it came from a fictional exchange called TOUCOIN, with a fictional trading pair FXR/LIEN. We have provided both SWAP and SPOT data for this pair, over the 2 month period 2020-07-15 to 2020-09-15.
	1. As the competition has not yet started, only SPOT sample data from 2020-05-01 to 2020-06-30 has been provided.

2. Your strategy must trade ONLY using SPOT, however you are provided with SWAP to use as part of predictions should you choose to.

3. Strategies will be ranked solely on their PnL over the judging data.

4. Competitors will receive a summary of how their strategy performed on the judging data, as well as their rank among all other participants

5. Top competitors will receive more detailed information about how their strategy performed, including a graphic representation

## Setup

For the purpose of this competition, please use the following license info:

"license\_id":"TRAIL001",

"license\_key":"apifiny123456"

### On your own computer

It is advised to skip to the "Using our server" section if you wish to get started quickly with minimal difficulty.

#### Docker

> **Warning**
>
>
> Docker storage is NOT persistent by default. Additionally, to submit your code, you will have to rsync it to our server.
> Make sure to be mindful of this fact, and do not lose your hard work!

1. Download [Docker](https://docs.docker.com/get-docker/)

2. Run the Algo SDK docker image:
```
docker run -it apifinyalgo/algo-sdk:1.1.0
```
You may need to use sudo here, depending on OS.

> ${ALGO_HOME}
>
>
> Your ALGO_HOME for this docker image will be /data/cc/, as that is where it is downloaded.
> Be sure to change this in your environment, and you will also find the sdk here.

3. Make a directory for your code, e.g. ```cd home; mkdir dev```

4. For a quickstart, clone this repository
``` git clone https://gitlab.com/william.qin/cornell-trading-competition.git ```

5. Fetch our data via rsync ```rsync -av -e 'ssh -p 2023' maryjane@38.142.207.130:/prod/data/toucoin/ data/toucoin```

6. Set up your environment as provided for in "Using our server", steps 3 and beyond.

7. Get started! See the "Writing your code" section for more details.

#### Ubuntu 20.04

1. To download Algo:
```
curl https://algoserver.apifiny.com/static/download/release/algo_sdk_1.1.0.tar.gz -o algo_sdk_1.1.0.tar.gz
tar -zxvf algo_sdk_1.1.0.tar.gz
```
2. Proceed as above.

### Using our server

Apifiny will provide a server for use by you, the hackers, over the course of the competition. During the competition period, there will be a calendar to use to sign up to use the server, and each competitor will receive a separate user account. For now, we have provided the maryjane user account for your use; the password is "johndoe".

1. To login, type ```ssh -p 2023 maryjane@38.142.207.130``` into a terminal window, and provide the password "johndoe" when prompted.

2. In the home folder (use the ```ls``` command to show its contents), you will see a folder labelled "submission". Each competitor will have a copy of this in their home folders, and these will be used to submit code.

3. To set up the environment, run the following commands, replacing ALGO_HOME with the path to your algo_sdk (on our server, it is at /prod/algo_sdk):

```
export ALGO_HOME=/prod/algo_sdk
export TZ=UTC
export LD_LIBRARY_PATH=${ALGO_HOME}/bin:$LD_LIBRARY_PATH
export PATH=${ALGO_HOME}/bin:$PATH
export PATH=${ALGO_HOME}/scripts:$PATH

export PATH=${HOME}/bin:$PATH
```

You may wish to save this to your ~/.bashrc (This has already been done for maryjane)

4. There is a default configuration file provided, maker.json. To run it, enter the command ```ccc_sim_trader examples/maker.json 20200501```.

5. To run your strategy over a date range, use ```gen_dates.py -sd 20200501 -ed 20200701 | parallel -j 64 ccc_sim_trader examples/maker.json```

6. To get summary statistics of how your strategy did, run ```sim_ana.py -sd 20200501 -ed 20200701 -p logs```


### Submission

1. To submit your strategy, place your C++ code (if applicable) and your configuration file into one folder on your own computer. We will assume you called it dir1.

2. Run the following command: ```rsync -a -e "ssh -p 2023" dir1 maryjane@38.142.207.130:~/submission/``` on your own computer.

3. Naming convention: Config files are to be named "[COMPETITOR_ID].json". C++ strategy files are to be named "[COMPETITOR_ID]-strat-[CLASS_NAME].cpp" and "[COMPETITOR_ID]-strat-[CLASS_NAME].h". Exactly one config file and one C++ strategy file (if applicable) can be submitted per competitor. If multiple are submitted, the most recently edited one(s) will be selected. If you wish to submit further C++ files (e.g. variables), make a folder within the "submission" folder called "xlibs" and place the compiled binary files there. 

## Writing your code

### Running your strategy

In general, you can run your strategy with the command ```ccc_sim_trader path/to/config/file date```, where the date is in the YYYYMMDD format.

### C++ code

In the examples folder, you will find some C++ files. You can edit them and add your own. Once you have done so, to run your strategy:

1. in CCMain.cpp, import your strategy file.

2. In getStrategy, add a new condition to the if statement, with your strategy name in place of the default.

3. Build your strategy, by running in your base directory ```sh build_scripts/rebuild.sh```

4. Run it by replacing "ccc_sim_trader" with "strat" in the relevant command.

<!---
"symbol_info": {
        "FXRLIEN.TOUCOIN": {"base_coin": "LIEN", "coin": "FXR", "local_name": "FXRLIEN", "lotsize": 1e-08, "min_order_size": 1e-08, "multiple": 1.0, "qty_precision": 8.0, "ticksize": 0.001},
        "FXRLIEN.TOUCOIN": {"base_coin": "LIEN", "coin": "FXR", "local_name": "FXRLIEN", "lotsize": 1e-08, "min_order_size": 1e-08, "multiple": 1.0, "qty_precision": 8.0, "ticksize": 0.001}
},

["CSV10Player", {"port": ["FXRLIEN", "TOUCOIN"], "path": "/prod/data/toucoin"}]
["CSV10Player", {"port": ["FXRLIENSWAP", "TOUCOIN_SWAP"], "path": "/prod/data/toucoin"}]

"symbol_info": {
        "FXRLIEN.TOUCOIN": {"base_coin": "LIEN", "coin": "FXR", "local_name": "FXR/LIEN", "lotsize": 1e-08, "min_order_size": 1e-08, "multiple": 1.0, "qty_precision": 8.0, "ticksize": 0.001}
    },
    "symbols": [
        {"cid": 1001, "port": ["FXRLIEN", "TOUCOIN"]}
    ],
    "players": [
        ["SpotPlayer", ["CSV10Player", {"port": ["FXRLIEN", "TOUCOIN"], "path": "/prod/data/toucoin"}]],
        ["SwapPlayer", ["CSV10Player", {"port": ["FXRLIENSWAP", "TOUCOIN_SWAP"], "path": "/prod/data/toucoin"}]]
    ],
-->
